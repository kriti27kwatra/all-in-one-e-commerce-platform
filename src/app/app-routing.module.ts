import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from './shared/components/not-found/not-found.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {path: '', redirectTo: '/products', pathMatch: 'full'},
      {
        path: 'products',
        loadChildren: () => import('./products/products.module').then(m => m.ProductsModule)
      },
      {
        path: 'login',
        loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
      },
      {
        path: 'cart',
        loadChildren: () => import('./order/order.module').then(m => m.OrderModule)
      },
      {
      path: 'notfound',
      component: NotFoundComponent,
      },
      {
        path: '**',
        component: NotFoundComponent,
     }
  ],
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
