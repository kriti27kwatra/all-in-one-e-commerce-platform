import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { DataShareService } from 'src/app/shared/services/data-share.service';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.scss']
})
export class UserLoginComponent implements OnInit {

  constructor(private location: Location, private dataShareService: DataShareService,) { }

  ngOnInit(): void {
    window.localStorage.clear();
  }

  submit(form): void {
    const user = {
      name: form.UserName,
      password: form.Password
    };
    window.localStorage.setItem('user', JSON.stringify(user));
    this.dataShareService.setLoggedInUser();
    this.location.back();
  }
}
