import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CommonService } from './common.service';
import { map, catchError } from 'rxjs/operators';
import { AppConstants } from 'src/app/app.constants';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  constructor(private http: HttpClient, private commonService: CommonService) { }
  getCartItems(productIds: string): Observable<any> {
    return this.http.get<any>(`${this.getApiUrl()}/cart?productIds=${productIds}`)
      .pipe(map((response) =>  this.commonService.extractData(response)), catchError(this.commonService.handleError));
}
  private getApiUrl() {
    return AppConstants.API_URL;
  }
}
