import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CommonService } from './common.service';
import { map, catchError } from 'rxjs/operators';
import { AppConstants } from 'src/app/app.constants';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient, private commonService: CommonService) { }
  getProducts(categoty: string=''): Observable<any> {
    return this.http.get<any>(`${this.getApiUrl()}/product?category=${categoty}`)
      .pipe(map((response) =>  this.commonService.extractData(response)), catchError(this.commonService.handleError));
}

getProductDetails(productId: number): Observable<any> {
  return this.http.get<any>(`${this.getApiUrl()}/product/${productId}`)
    .pipe(map((response) =>  this.commonService.extractData(response)), catchError(this.commonService.handleError));
}

getAllCategories(): Observable<any> {
  return this.http.get<any>(`${this.getApiUrl()}/product/category`)
    .pipe(map((response) =>  this.commonService.extractData(response)), catchError(this.commonService.handleError));
}

searchProducts(searchValue: string, categoty: string=''): Observable<any> {
  return this.http.get<any>(`${this.getApiUrl()}/product/search?title=${searchValue}&category=${categoty}`)
    .pipe(map((response) =>  this.commonService.extractData(response)), catchError(this.commonService.handleError));
}

  private getApiUrl() {
    return AppConstants.API_URL;
  }
}
