import { inject, TestBed } from '@angular/core/testing';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CommonService } from './common.service';
import { DataShareService } from './data-share.service';
import { ProductService } from './product.service';
import { MessageService } from 'primeng/api';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable, of, from, throwError } from 'rxjs';
import 'rxjs/add/observable/of';

fdescribe('ProductService', () => {
  let productService: ProductService;
  let httpClient: HttpClient;
  let commonService: CommonService;
  let dataShareService: DataShareService;
  let messageService: MessageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule],
      providers: [HttpClient, CommonService,
        DataShareService, MessageService],
    });
  });

  beforeEach(inject([ProductService, HttpClient, CommonService],
    (_productService: ProductService, _httpclient: HttpClient, _commonService: CommonService) => {
      productService = _productService;
      commonService = _commonService;
      httpClient = _httpclient;
    }));

  it('should be created', () => {
    expect(productService).toBeTruthy();
  });

  it('should be able to get products list', () => {
    const result = [
      {
        id: 1,
        title: 'Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops',
        price: 109.95,
        description: 'Your perfect pack for everyday use and walks in the forest.',
        category: 'Mens clothing',
        quantity: 0,
        imageUrl: 'https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg',
        tags: ['Backpack', 'Laptop Bag', 'Dark Blue', 'Traveller Bag'],
        inCartFlag: false
      },
      {
        id: 2,
        title: 'Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops',
        price: 19.95,
        description: 'Your perfect pack for everyday use and walks in the forest.',
        category: 'Womens clothing',
        quantity: 10,
        imageUrl: 'https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg',
        tags: ['Backpack', 'Laptop Bag', 'Dark Blue', 'Traveller Bag'],
        inCartFlag: false
      }
    ];
    spyOn(httpClient, 'get').and.returnValue(of(result));
    spyOn(commonService, 'extractData').and.returnValue(result);

    productService.getProducts().subscribe(
      (data) => {
        expect(data).toEqual(result);
      });
  });

  it('should be able to categories list', () => {
    const result = [
      {
        id: 1,
        name: 'Electronics'
      },
      {
        id: 2,
        name: 'Jewelery'
      },
      {
        id: 3,
        name: 'Mens clothing'
      },
      {
        id: 4,
        name: 'Womens clothing'
      }

    ];
    spyOn(httpClient, 'get').and.returnValue(of(result));
    spyOn(commonService, 'extractData').and.returnValue(result);

    productService.getAllCategories().subscribe(
      (data) => {
        expect(data).toEqual(result);
      });
  });

  it('should be able to get products details based on product Id', () => {
    const result = [
      {
        id: 2,
        title: 'Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops',
        price: 19.95,
        description: 'Your perfect pack for everyday use and walks in the forest.',
        category: 'Womens clothing',
        quantity: 10,
        imageUrl: 'https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg',
        tags: ['Backpack', 'Laptop Bag', 'Dark Blue', 'Traveller Bag'],
        inCartFlag: false
      }
    ];
    spyOn(httpClient, 'get').and.returnValue(of(result));
    spyOn(commonService, 'extractData').and.returnValue(result);

    productService.getProductDetails(1).subscribe(
      (data) => {
        expect(data).toEqual(result);
      });
  });

  it('should be able to search products based on search text', () => {
    const result = [
      {
        id: 2,
        title: 'Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops',
        price: 19.95,
        description: 'Your perfect pack for everyday use and walks in the forest.',
        category: 'Womens clothing',
        quantity: 10,
        imageUrl: 'https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg',
        tags: ['Backpack', 'Laptop Bag', 'Dark Blue', 'Traveller Bag'],
        inCartFlag: false
      }
    ];
    spyOn(httpClient, 'get').and.returnValue(of(result));
    spyOn(commonService, 'extractData').and.returnValue(result);

    productService.searchProducts('Backpack').subscribe(
      (data) => {
        expect(data).toEqual(result);
      });
  });
});
