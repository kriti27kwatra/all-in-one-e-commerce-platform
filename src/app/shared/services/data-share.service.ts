import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Cart } from '../models/cart.model';
import { CommonService } from './common.service';
@Injectable({
  providedIn: 'root'
})
export class DataShareService {
  item: Cart = {
    cartItems: []
  };
  private cartList = new BehaviorSubject(this.item);
  // eslint-disable-next-line @typescript-eslint/member-ordering
  cartValueEvent = this.cartList.asObservable();
  private loggedInUser = new BehaviorSubject(null);
  // eslint-disable-next-line @typescript-eslint/member-ordering
  userValueEvent = this.loggedInUser.asObservable();


  constructor(private commonService: CommonService) { this.setLoggedInUser(); }

  setLoggedInUser(): void {
    if(window.localStorage.getItem('user')) {
      const user = JSON.parse(window.localStorage.getItem('user'));
      this.loggedInUser.next(user.name);
    }
    else {
      this.loggedInUser.next(null);
    }
  }
  addProductToCart(id: number) {
      const item = this.cartList.getValue();
      const prdIndex = item.cartItems.findIndex((ele)=> ele.productId === id);
      if (prdIndex > -1) {
        item.cartItems[prdIndex].quantity++;
      }
      else {
        item.cartItems.push({
          productId: id,
          quantity: 1
        });
      }
      this.cartList.next(item);
  }

  emptyCart(): void {
    const item = this.cartList.getValue();
    item.cartItems.splice(0,item.cartItems.length);
    this.cartList.next(this.item);
  }
  removeProductFromCart(id: number) {
    const item = this.cartList.getValue();
    const prdIndex = item.cartItems.findIndex((ele)=> ele.productId === id);
    if (prdIndex > -1) {
      if (item.cartItems[prdIndex].quantity > 1) {
        item.cartItems[prdIndex].quantity--;
      }
      else {
        item.cartItems.splice(prdIndex,1);
      }
    }
    this.cartList.next(item);
}
}
