import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';
import { throwError } from 'rxjs';
import { AppConstants } from '../../app.constants';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private messageService: MessageService) { }

  handleError(response: any): any {
    if (response && response.status === 500) {
        return throwError(response);
    } else if (response && response.status === 403) {
        return throwError(AppConstants.NOT_AUTHORIZED);
    } else if (response && response.status === 404) {
        return throwError(response);
    } else {
        return throwError(AppConstants.DEFAULT_ERROR);
    }
}

extractData(data: any, isResultOnly: boolean = true) {
    if (typeof (data.isSuccess) !== 'undefined') {
        if (data.isSuccess) {
            return isResultOnly ? data.results : data;
        } else if (data.errors && data.errors.length > 0) {
            const details = { detail: data.errors[0] };
            return throwError(details);
        } else {
            return throwError(AppConstants.DEFAULT_ERROR);
        }
    } else {
        return data;
    }
}

showOrderSuccess(successMsg: any) {
    this.messageService.clear();
    this.messageService.add({severity:'success', summary: successMsg});
}

showError(errorMsg: any) {
    this.messageService.clear();
    this.messageService.add({severity:'error', summary: errorMsg});
}

showwarning(warningMsg: any) {
    this.messageService.clear();
    this.messageService.add({severity:'warn', summary: warningMsg});
}
}
