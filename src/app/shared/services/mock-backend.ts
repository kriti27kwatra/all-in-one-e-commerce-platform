import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, of, throwError, BehaviorSubject } from 'rxjs';
import { delay, mergeMap, map, materialize, dematerialize } from 'rxjs/operators';
import * as productsJsonData from '../../../assets/mock-data/Products.json';
import * as categoriesJsonData from '../../../assets/mock-data/Categories.json';
import { Product } from '../models/product.model';
import { Category } from '../models/category.model';

@Injectable()
export class MockBackendInterceptor implements HttpInterceptor {
    productList: BehaviorSubject<Product[]>;
    categoryList: BehaviorSubject<Category[]>;

    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // wrap in delayed observable to simulate server api call
        return of(null).pipe(mergeMap(() => {
            if (request.url.includes('/product')) {
                    if (request.url.endsWith('/product/category') && request.method === 'GET') { // get categories list
                        if (this.categoryList) {
                            return of(new HttpResponse({ status: 200, body: this.categoryList.getValue() }));
                        } else {
                            this.categoryList = new BehaviorSubject<Category[]>(new Array<Category>());
                            const categoriesData: any = (categoriesJsonData as any).default;
                            if (!categoriesData) {
                                return throwError({ status: 404, error: { message: `There are no Categories` } });
                            }
                            this.categoryList.next(categoriesData.category);
                            return of(new HttpResponse({ status: 200, body: this.categoryList.getValue() }));
                        }
                    }
                    else if (request.url.endsWith('/product?category=') && request.method === 'GET') {  // get all products
                        if (this.productList) {
                            return of(new HttpResponse({ status: 200, body: this.productList.getValue() }));
                        } else {
                            this.productList = new BehaviorSubject<Product[]>(new Array<Product>());
                            const productData: any = (productsJsonData as any).default;
                            if (!productData) {
                                return throwError({ status: 404, error: { message: `There are no Products` } });
                            }
                            this.productList.next(productData.products);
                            return of(new HttpResponse({ status: 200, body: this.productList.getValue() }));
                        }
                    }
                    else if (request.url.includes('/product?category=') && !request.url.endsWith('/product?category=')
                     && request.method === 'GET') {  // get products of any specific category
                        const categoryParam = request.url.split('=');
                        const categoriesId = categoryParam[categoryParam.length-1].split(',');
                        if(this.categoryList) {
                            const categories = this.categoryList.getValue();
                            const categoriesNames = [];
                            categoriesId.find((ele) => {
                                categories.find((cat)=> {
                                    if(cat.id === parseInt(ele, 10)) {
                                        categoriesNames.push(cat.name);
                                    }
                                });
                            });
                            if(this.productList) {
                                const products = this.productList.getValue();
                                const result = [];
                                products.find((ele)=> {
                                    categoriesNames.find((cat) => {
                                        if(cat === ele.category) {
                                            result.push(ele);
                                        }
                                    });
                                });
                                if (result.length > 0) {
                                     return of(new HttpResponse({ status: 200, body: result }));
                                }
                                else {
                                    return throwError({ status: 404, error: { message: `Products with these ctegories are not found` } });
                                }
                            }
                        }
                    }
                    else if (request.url.includes('/product/search?title') && request.url.includes('&category=')
                     && request.method === 'GET') {  // get products of any specific category
                        const titleIndex = request.url.indexOf('?title=');
                        const categoryIndex = request.url.indexOf('&category=');
                        const searchText = request.url.substring(titleIndex + 7, categoryIndex);
                        const categoriesId = request.url.substring(categoryIndex + 10, request.url.length).split(',');
                        if (categoriesId.length > 0 && categoriesId[0] !== '') {
                            if(this.categoryList) {
                                const categories = this.categoryList.getValue();
                                const categoriesNames = [];
                                categoriesId.find((ele) => {
                                    categories.find((cat)=> {
                                        if(cat.id === parseInt(ele, 10)) {
                                            categoriesNames.push(cat.name);
                                        }
                                    });
                                });
                                if(this.productList) {
                                    const products = this.productList.getValue();
                                    const categoryresult = [];
                                    products.find((ele)=> {
                                        categoriesNames.find((cat) => {
                                            if(cat === ele.category) {
                                                categoryresult.push(ele);
                                            }
                                        });
                                    });
                                    const result = [];
                                    categoryresult.find((ele)=> {
                                        if(ele.title.toLowerCase().includes(searchText.toLowerCase())) {
                                            result.push(ele);
                                        }
                                    });
                                    if (result.length > 0) {
                                         return of(new HttpResponse({ status: 200, body: result }));
                                    }
                                    else {
                                        return throwError({ status: 404, error:
                                            { message: `Products with these categories are not found` } });
                                    }
                                }
                            }
                        }
                        else {
                            if(this.productList) {
                                const products = this.productList.getValue();
                                const result = [];
                                products.find((ele)=> {
                                    if(ele.title.toLowerCase().includes(searchText.toLowerCase())) {
                                        result.push(ele);
                                    }
                                });
                                if (result.length > 0) {
                                    return of(new HttpResponse({ status: 200, body: result }));
                               }
                               else {
                                   return throwError({ status: 404, error: { message: `Products with this title are not found` } });
                               }
                            }
                        }
                    }
                    else if (request.url.includes('/product/') && request.method === 'GET') { // get details of product with id
                        const index = request.url.lastIndexOf('/');
                        const id = request.url.substr(index+1);
                        if(!this.productList) {
                            this.productList = new BehaviorSubject<Product[]>(new Array<Product>());
                            const productData: any = (productsJsonData as any).default;
                            if (!productData) {
                                return throwError({ status: 404, error: { message: `There are no Products` } });
                            }
                            this.productList.next(productData.products);
                            const products = this.productList.getValue();
                            const result = products.find((ele)=> ele.id === parseInt(id, 10));
                            if (result) {
                                return of(new HttpResponse({ status: 200, body: result }));
                           }
                           else {
                               return throwError({ status: 404, error: { message: `Products with this id is not found` } });
                           }
                        }
                        else {
                            const products = this.productList.getValue();
                            const result = products.find((ele)=> ele.id === parseInt(id, 10));
                            if (result) {
                                return of(new HttpResponse({ status: 200, body: result }));
                           }
                           else {
                               return throwError({ status: 404, error: { message: `Products with this id is not found` } });
                           }
                        }
                    }
                }
            else if (request.url.includes('/cart')) {
                if (request.url.includes('/cart?productIds') && request.method === 'GET') { // get cart Items
                    const productIdsParam = request.url.split('=');
                    const productIds = productIdsParam[productIdsParam.length-1].split(',');
                        if(this.productList) {
                            const products = this.productList.getValue();
                            const result = [];
                            products.find((ele)=> {
                                productIds.find((id) => {
                                    if(parseInt(id, 10) === ele.id) {
                                        result.push(ele);
                                    }
                                });
                            });
                            if (result.length > 0) {
                                 return of(new HttpResponse({ status: 200, body: result }));
                            }
                            else {
                                return throwError({ status: 404, error: { message: `Cart Items not found` } });
                            }
                        }
                }
            }
            return next.handle(request);
            }))
            // call materialize and dematerialize to ensure delay even if an error is thrown
            .pipe(materialize())
            .pipe(delay(500))
            .pipe(dematerialize());
    }
}
