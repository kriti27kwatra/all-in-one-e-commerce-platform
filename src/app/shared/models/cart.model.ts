import { CartItem } from './cartItem.model';

export class Cart {
    userId?: number;
    cartItems = new Array<CartItem>();
}
