export class Product {
    id: number;
    title: string;
    category: string;
    quantity: number;
    description: string;
    imageUrl: string;
    price: number;
    tags: string[];
    updatequantity?: number;
    inCartFlag?: boolean = false;
}
