import { Product } from './product.model';

export class CartDetail {
    productDetail: Product;
    quantityInCart: number;
}
