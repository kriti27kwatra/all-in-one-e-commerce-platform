import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ToolbarModule } from 'primeng/toolbar';
import {ButtonModule} from 'primeng/button';
import {SplitButtonModule} from 'primeng/splitbutton';
import {BadgeModule} from 'primeng/badge';
import {FormsModule} from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {MultiSelectModule} from 'primeng/multiselect';
import {CardModule} from 'primeng/card';
import { TagModule } from 'primeng/tag';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MockBackendInterceptor } from './services/mock-backend';
import {InputTextModule} from 'primeng/inputtext';
import {PanelModule} from 'primeng/panel';
import {OrderListModule} from 'primeng/orderlist';
import {ToastModule} from 'primeng/toast';
import { MessageService } from 'primeng/api';

@NgModule({
  declarations: [
    NotFoundComponent
  ],
  imports: [
    CommonModule,
    ToolbarModule,
    ButtonModule,
    SplitButtonModule,
    BadgeModule,
    FormsModule,
    DropdownModule,
    AutoCompleteModule,
    MultiSelectModule,
    CardModule,
    TagModule,
    HttpClientModule,
    InputTextModule,
    PanelModule,
    OrderListModule,
    ToastModule
  ],
  exports: [
    ToolbarModule,
    ButtonModule,
    SplitButtonModule,
    BadgeModule,
    FormsModule,
    DropdownModule,
    AutoCompleteModule,
    MultiSelectModule,
    CardModule,
    TagModule,
    HttpClientModule,
    InputTextModule,
    PanelModule,
    OrderListModule,
    ToastModule
  ],
  providers: [
    MessageService,
    { provide: HTTP_INTERCEPTORS, useClass: MockBackendInterceptor, multi: true },
  ]
})
export class SharedModule {
}
