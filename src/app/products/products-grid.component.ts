import { Component, OnInit } from '@angular/core';
import { Category } from '../shared/models/category.model';
import { Product } from '../shared/models/product.model';
import { ProductService } from '../shared/services/product.service';

@Component({
  selector: 'app-products-grid',
  templateUrl: './products-grid.component.html',
  styleUrls: ['./products-grid.component.scss']
})
export class ProductsGridComponent implements OnInit {
  productList: Product[];
  categoryList: Category[];
  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    this.getAllProducts();
    this.getAllCategories();
  }

  getAllProducts(selectedProductCategory: string = ''): void {
    this.productService.getProducts(selectedProductCategory).subscribe((result: Product[]) => {
      this.productList = result;
    });
  }

  getAllCategories(): void {
    this.productService.getAllCategories().subscribe((result: Category[]) => {
      this.categoryList = result;
    });
  }

  categorySelectedEvent(event: any): void {
    this.getAllProducts(event.selectedProductCategory);
  }

  searchProductEvent(event: any): void {
    this.productService.searchProducts(event.searchValue, event.selectedProductCategory).subscribe((result: Product[]) => {
      this.productList = result;
    });
  }
}
