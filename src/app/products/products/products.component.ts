import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from 'src/app/shared/models/product.model';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  @Input() product: Product;
  constructor(private route: Router) { }

  ngOnInit(): void {
  }

  productDetails(): void {
    this.route.navigate([`products/${this.product.id}`]);
  }
}
