import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { Category } from 'src/app/shared/models/category.model';

@Component({
  selector: 'app-search-product',
  templateUrl: './search-product.component.html',
  styleUrls: ['./search-product.component.scss']
})
export class SearchProductComponent implements OnInit {
  @Input() categoryList: Category[];
  @Output() categorySelected = new EventEmitter();
  @Output() search = new EventEmitter();
  searchValue;
  productCategoryValues: SelectItem[] = [];
  selectedProductCategory: any[] = [];

  constructor() { }

  ngOnInit(): void {
    this.getProductCategoryValues();
  }

  getProductCategoryValues(): void {
    this.categoryList.forEach(element => {
      this.productCategoryValues.push({
        label: element.name, value: element.id
      });
    });
  }
  searchProduct(): void {
    if (this.searchValue) {
        if(this.selectedProductCategory.length > 0) {
          const categories = this.selectedProductCategory.join();
          this.search.emit({ selectedProductCategory: categories, searchValue: this.searchValue });
        }
        else {
          this.search.emit({ selectedProductCategory: '', searchValue: this.searchValue });
        }
    }
  }

  onCategoryChange(): void {
      this.searchValue = '';
      const categories = this.selectedProductCategory.join();
      this.categorySelected.emit({ selectedProductCategory: categories });
  }
}
