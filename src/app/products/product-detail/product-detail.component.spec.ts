import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { DataShareService } from 'src/app/shared/services/data-share.service';
import { ProductService } from 'src/app/shared/services/product.service';
import { ProductDetailComponent } from './product-detail.component';
import { CommonService } from 'src/app/shared/services/common.service';
import { MessageService } from 'primeng/api';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of, from, throwError } from 'rxjs';
import 'rxjs/add/observable/of';

fdescribe('ProductDetailComponent', () => {
  let component: ProductDetailComponent;
  let fixture: ComponentFixture<ProductDetailComponent>;
  let productService: ProductService;
  let dataShareService: DataShareService;
  let commonService: CommonService;
  let messageService: MessageService;
  let activatedRoute: ActivatedRoute;

  const router = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        ProductService,
        DataShareService,
        CommonService,
        MessageService,
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({
              id: 2
            }),
          }
        },
        { provide: Router, useValue: router }
      ],
      declarations: [ ProductDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductDetailComponent);
    component = fixture.componentInstance;
    productService = fixture.debugElement.injector.get(ProductService);
    dataShareService = fixture.debugElement.injector.get(DataShareService);
    commonService = fixture.debugElement.injector.get(CommonService);
    messageService = fixture.debugElement.injector.get(MessageService);
    activatedRoute = fixture.debugElement.injector.get(ActivatedRoute);

    const details = {
      id: 1,
      title: 'Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops',
      price: 109.95,
      description: 'Your perfect pack for everyday use and walks in the forest.',
      category: 'Mens clothing',
      quantity: 0,
      imageUrl: 'https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg',
      tags: ['Backpack', 'Laptop Bag', 'Dark Blue', 'Traveller Bag'],
      inCartFlag: false
    };
    spyOn(productService, 'getProductDetails').and.returnValue(Observable.of(details));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be to fetch product details using product service', () => {
    component.getProductDetails();
    expect(productService.getProductDetails).toHaveBeenCalled();
  });

  it('should be to fetch product details using product service', () => {
    component.getProductDetails();
    expect(productService.getProductDetails).toHaveBeenCalled();
  });

  it('should not set the product details when empty result is returned from product Service', () => {
    productService.getProductDetails = jasmine.createSpy().and.returnValue(Observable.of(null));
    component.getProductDetails();
    expect(productService.getProductDetails).toHaveBeenCalled();
  });
  it('throw error - 404 while getting product details', () => {
    productService.getProductDetails = jasmine.createSpy().and.returnValue(throwError({ status: 404 }));
    component.getProductDetails();
    expect(productService.getProductDetails).toHaveBeenCalled();
  });

  it('should be able add product to cart', () => {
    dataShareService.addProductToCart = jasmine.createSpy().and.returnValue(null);
    component.addOrRemoveProduct();
    expect(dataShareService.addProductToCart).toHaveBeenCalled();
  });

  it('should be able remove product from cart', () => {
    component.productDetails = {
      id: 1,
      title: 'Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops',
      price: 109.95,
      description: 'Your perfect pack for everyday use and walks in the forest.',
      category: 'Mens clothing',
      quantity: 0,
      imageUrl: 'https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg',
      tags: ['Backpack', 'Laptop Bag', 'Dark Blue', 'Traveller Bag'],
      inCartFlag: true
    };
    dataShareService.removeProductFromCart = jasmine.createSpy().and.returnValue(null);
    component.addOrRemoveProduct();
    expect(dataShareService.removeProductFromCart).toHaveBeenCalled();
  });
});
