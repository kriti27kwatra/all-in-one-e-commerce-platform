import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/shared/models/product.model';
import { DataShareService } from 'src/app/shared/services/data-share.service';
import { ProductService } from 'src/app/shared/services/product.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  productId: number;
  productDetails: Product;
  constructor(private productService: ProductService, private route: ActivatedRoute, private dataShareService: DataShareService,
              private router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.productId = params.id;
    });
    this.getProductDetails();
  }

  getProductDetails(): void {
    this.productService.getProductDetails(this.productId).subscribe((result: Product) => {
      this.productDetails = result;
    }, (error) => {
      if (error.status === 404) {
        this.router.navigate(['notfound']);
      }
    });
  }

  addOrRemoveProduct(): void {
    if (!this.productDetails.inCartFlag) {
      const result = this.dataShareService.addProductToCart(this.productId);
      this.productDetails.quantity--;
    }
    else {
      this.dataShareService.removeProductFromCart(this.productId);
      this.productDetails.quantity++;
    }
    this.productDetails.inCartFlag = !this.productDetails.inCartFlag;
  }
}
