import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartDetail } from 'src/app/shared/models/cart-detail.model';
import { Cart } from 'src/app/shared/models/cart.model';
import { Product } from 'src/app/shared/models/product.model';
import { CommonService } from 'src/app/shared/services/common.service';
import { DataShareService } from 'src/app/shared/services/data-share.service';
import { OrderService } from 'src/app/shared/services/order.service';

@Component({
  selector: 'app-view-cart',
  templateUrl: './view-cart.component.html',
  styleUrls: ['./view-cart.component.scss']
})
export class ViewCartComponent implements OnInit {
  cart: Cart;
  cartDetails: CartDetail[];
  orderAmount = 0;
  constructor(private dataShareService: DataShareService, private route: Router, private orderService: OrderService,
    private commonService: CommonService) { }

  ngOnInit(): void {
    this.dataShareService.cartValueEvent.subscribe(cart => this.cart = cart);
    this.getCartItems();
  }

  getCartItems(): void {
    const productIds = [];
    this.cart.cartItems.forEach((item) => {
      productIds.push(item.productId);
    });
    this.orderService.getCartItems(productIds.join()).subscribe((result: Product[]) => {
      this.cartDetails = new Array<CartDetail>();
      result.forEach((product) => {
        const cartDetail = new CartDetail();
        cartDetail.productDetail = product;
        cartDetail.quantityInCart = 1;
        cartDetail.productDetail.updatequantity = 1;
        this.cartDetails.push(cartDetail);
        this.computeOrderSummary();
      });
    });
  }
  placeOrder(): void {
    this.route.navigate(['/cart/checkout']);
  }

  computeOrderSummary(): void {
    this.orderAmount = 0;
    this.cartDetails.forEach((item) => {
      this.orderAmount = this.orderAmount + (item.productDetail.price * item.quantityInCart);
    });
  }

  updateProductquantity(prd: CartDetail, quantity: number): void {
    this.cartDetails.forEach((item) => {
      if(prd.productDetail.id === item.productDetail.id) {
        const diff = quantity - prd.quantityInCart;
        if (diff > 0) { // increase quantity of product
          if(item.productDetail.quantity >= diff) {
            item.quantityInCart = item.quantityInCart + diff;
            item.productDetail.quantity = item.productDetail.quantity - diff;
            this.computeOrderSummary();
          }
          else {
            this.commonService.showError('Required quantity is not available');
          }
        }
        else { // decrease quantity of product
          item.quantityInCart = item.quantityInCart - Math.abs(diff);
          item.productDetail.quantity = item.productDetail.quantity + Math.abs(diff);
          this.computeOrderSummary();
        }
      }
    });
  }
}
