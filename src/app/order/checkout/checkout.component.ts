import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/services/common.service';
import { DataShareService } from 'src/app/shared/services/data-share.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {

  constructor(private commonService: CommonService, private route: Router, private dataShareService: DataShareService) { }

  ngOnInit(): void {
  }
  submit(form): void {
    const msg = 'Your Order is Successfully Placed!';
    this.commonService.showOrderSuccess(msg);
    this.dataShareService.emptyCart();
    this.route.navigate(['/products']);
  }
}
