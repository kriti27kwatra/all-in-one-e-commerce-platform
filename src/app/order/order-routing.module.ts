import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../shared/guards/auth.guard';
import { CheckoutComponent } from './checkout/checkout.component';
import { ViewCartComponent } from './view-cart/view-cart.component';

const routes: Routes = [
  {
        path: '',
        canActivate: [AuthGuard],
        children: [
          {
            path: '',
            component: ViewCartComponent,
          },
          {
            path: 'checkout',
            component: CheckoutComponent
          }
        ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderRoutingModule { }
