import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppConstants } from 'src/app/app.constants';
import { Cart } from './shared/models/cart.model';
import { DataShareService } from './shared/services/data-share.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title: string;
  userName;
  cartCount: number;
  constructor(private dataShareService: DataShareService, private route: Router) { }

  ngOnInit(): void {
    this.getUser();
    this.dataShareService.cartValueEvent.subscribe(cart => this.cartCount = cart.cartItems.length);
    this.dataShareService.userValueEvent.subscribe(user => this.userName = user);
    this.title = AppConstants.APP_TITLE;
  }

  viewCart(): void {
    if (this.cartCount > 0) {
      this.route.navigate(['/cart']);
    }
  }

  getUser(): void {
    if(window.localStorage.getItem('user')) {
      const user = JSON.parse(window.localStorage.getItem('user'));
      this.userName = user.name;
    }
  }

  login(): void {
    this.route.navigate(['/login']);
  }

  goHome(): void {
    this.route.navigate(['/products']);
  }
}
