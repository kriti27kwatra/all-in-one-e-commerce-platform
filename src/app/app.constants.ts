const AppConstants = {
    APP_TITLE: 'All In One E-commerce Platform',
    DEFAULT_ERROR: 'Error occurred, please try again',
    NOT_AUTHORIZED: 'Not authorized to perform this action',
    API_URL: 'api'
};

export {AppConstants};
