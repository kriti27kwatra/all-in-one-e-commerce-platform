# All In One E-commerce Platform

			NAGP_Angular Workshop Home Assignment

Please see the following details for the All-In-One E-commerce Application:
1.	Code Base Link: https://gitlab.com/kriti27kwatra/all-in-one-e-commerce-platform/ 
2.	Deployed Application Link: https://condescending-banach-8ba0f5.netlify.app/ 
3.	Bonus Points:
    a.	Category Tree: Products are classified on the basis of Categories. User can filter products based on category and can also search product based on specific category.
    b.	Translation: Not Implemented
    c.	Auth Guard: Implemented and activated for Cart and Checkout pages.
    d.	Linting: Used ESLint for Linting the application
    e.	Backend: Created Mock Backend using Behaviour Subject and Mock Json Data. Refer the mock-backend.ts file for Mock Backend.
    f.	CSS Library: PrimeNG is used for CSS.
    g.	Toast and Message API: PrimeNG Toast Module and Message API is used to show error, warning or useful messages wherever required. 
    h.	NotFound Component: Not Found Page will be displayed in case user navigates outside application pages.
4.	Unit Test case: Unit test cases are written for the following:
    a.	Product Service
    b.	Product Detail Component

